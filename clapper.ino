bool state = false; //relay state
const int sampleWindow = 50; // Sample window width in mS (50 mS = 20Hz)
unsigned int sample; 
unsigned int clap1 = 0; //first clap millis() timestamp
unsigned int clap2 = 0; //second clap millis() timestamp

void setup() 
{
   Serial.begin(9600);
}

void loop() 
{
  int noise = micSample();//obtain 50 ms sample
  int test_sample = analogRead(0);//obtain raw sample
  if(noise>90){
    if(clap1 == 0){
      clap1 = millis();//set first clap timestamp
      return;
    }
    if(clap2==0){
      if(millis()-clap1<50){
        return;
      }
      clap2 = millis();
      }
      
      int diff = clap2 - clap1;
      Serial.println();
      Serial.print("clap1 ");
      Serial.println(clap1);
      Serial.print("clap2 ");
      Serial.println(clap2);   
      Serial.print("diff ");
      Serial.println(diff);
      Serial.println();
      if( diff >=300 && diff <=  1100){
        state = !state;
        Serial.print("state changed to ");
        Serial.println(state);
        delay(1000);
     }
   }
   //reset old claps
   if(millis()-clap1 > 1100){
    clap1 = 0;
   }
   if(millis()-clap2 > 1100){
    clap2 = 0;
   }
}

int micSample(){
  unsigned long startMillis= millis();  // Start of sample window
  // collect data for 50 mS
  while (millis() - startMillis < sampleWindow)
  {
    sample = analogRead(0);
    if(sample > 90){
    return sample;
  }
  return 0;
}
